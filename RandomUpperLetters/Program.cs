﻿// See https://aka.ms/new-console-template for more information
// Bir kısmı ters olsa da 5, 8, 13 fibonacci oldu :)
int _row = 13;
int _min = 5, _max = 8; // Actually max: 7

Random _random1 = new Random();
Random _random2 = new Random();

Result();

// A1
int RandomLength()
{
    lock (_random1) // synchronize
    {
        return _random1.Next(_min, _max);
    }
}

// A2
string RandomString(int length)
{
    lock (_random2) // synchronize
    {
        const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        return new string(Enumerable.Repeat(chars, length)
            .Select(s => s[_random2.Next(s.Length)]).ToArray());
    }
}

// A3
void Result()
{
    for (int i = 0; i < _row; i++)
    {
        Console.WriteLine(RandomString(RandomLength()));
    }
}

/*
Örnek çıktı:

6K093ZC
52HZET
ZZKP6
4MT29B
TULRR
CTXS5
C8GI89
HVQ5D
J8O3C
6QFTG
AUSSNNP
47F4E
PC3RBQ
 */
