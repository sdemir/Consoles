﻿var sonucListesi = [];
var start = 1, finish = 999999;
for (let i = 0; i < 10; i++) {
    sonucListesi.push(0);
}

// Kaç iterasyon yapalım test için?
// 9, 99, 999, 9999, 9999, ..., en son 999999999 deneyebilirsin doğru test için.
for (let i = start; i <= finish; i++) {
    let tempInt = Math.floor(Math.random() * finish) + 1;

    // İlk rakamını bulana kadar 10'a bölelim.
    while (tempInt >= 10) {
        tempInt = parseInt(tempInt / 10);
    }

    sonucListesi[tempInt]++;
}

// Sonuç inanılır gibi değil. Sihirli birşeyler var resmen :)
for (let i = start; i < 10; i++) {
    console.log(`${i}: ${sonucListesi[i]}`);
}