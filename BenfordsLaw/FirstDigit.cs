﻿// See https://aka.ms/new-console-template for more information

// Kolay olsun diye 10 verdim. Sıfırıncı indexi kullanmayacağım :)
long[] sonucListesi = new long[10];

// Instantiate random number generator using system-supplied value as seed.
var rand = new Random();
int tempInt;
// 9, 99, 999, 9999, 9999, ..., en son 999999999 deneyebilirsin doğru test için.
int start = 1, finish = 9999999;

// Kaç iterasyon yapalım test için?
for (int i = start; i <= finish; i++)
{
    tempInt = rand.Next(start, finish);

    // İlk rakamını bulana kadar 10'a bölelim.
    while (tempInt > 9)
    {
        tempInt = tempInt / 10;
    }

    sonucListesi[tempInt]++;
}

double toplam = 0, yuzde;
for (int i = start; i < 10; i++) toplam += sonucListesi[i];

// Sonuç inanılır gibi değil. Sihirli birşeyler var resmen :)
// > Düzeltiyorum, sonuç tahmin ettiğimiz gibi, yani sihirli bir durum yok! Hesaplama düzeltildi.
// > Gerçek dünyadaki elimizdeki verilerle sınamamız gerekiyor. Benford's Law onu iddia ediyor zaten. Algoritmik sınamada sınıfta kaldı.
for (int i = start; i < 10; i++)
{
    yuzde = (100 * sonucListesi[i]) / toplam;

    Console.WriteLine($"{i}: {sonucListesi[i]} (%{yuzde})");
}