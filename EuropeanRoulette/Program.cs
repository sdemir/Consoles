﻿namespace EuropeanRoulette
{
    internal class Program
    {
        const int _iteration = 100;
        static Random _rand;
        static bool _isOurChoiceOdd = false;
        static bool _isRollChoiceOdd = false;
        static int _oddCount = 0;
        static int _evenCount = 0;
        static int _winCount = 0;
        static int _loseCount = 0;
        static int _zeroCount = 0;
        static int _oddWinCount = 0;
        static int _evenWinCount = 0;
        static int _consecutiveLoss = 0;
        static int _maxConsecutiveLoss = 0;
        // Lose ve min arasındaki maksimum açılma. Kayıplara 0 da dahildir.
        static int _maxDiff = 0;
        static void Main(string[] args)
        {
            Initialization();
            int halfWay = _iteration / 2;
            for (int i = 1; i <= _iteration; i++)
            {
                RollDice();
                // Tüm denemelerin yarısındayken bir göz atalım.
                if (i == halfWay)
                {
                    Console.WriteLine("HALF WAY!");
                    Console.WriteLine($"Win/Iteration: {_winCount}/{i}");

                    Console.WriteLine($"OddWin/OddTotal: {_oddWinCount}/{_oddCount}");
                    Console.WriteLine($"EvenWin/EvenTotal: {_evenWinCount}/{_evenCount}");
                    Console.WriteLine($"Zero: {_zeroCount}");

                    Console.WriteLine($"MaxConsecutiveLoss: {_maxConsecutiveLoss}");
                    Console.WriteLine($"MaxDiff: {_maxDiff}");
                    Console.WriteLine("***********************************************");
                }
            }

            string print = string.Format($"Odd: {0}, Even: {1}, Win: {2}, Lose: {3}", _oddCount, _evenCount, _winCount, _loseCount);
            double winPercent = (double)_winCount / _iteration * 100;
            double oddWinPercent = (double)_oddWinCount / _iteration * 100;
            double evenWinPercent = (double)_evenWinCount / _iteration * 100;

            Console.WriteLine($"Win/Iteration: {_winCount}/{_iteration} (%{winPercent.ToString("0.###")})");

            Console.WriteLine($"OddWin/OddTotal: {_oddWinCount}/{_oddCount} (%{oddWinPercent.ToString("0.###")})");
            Console.WriteLine($"EvenWin/EvenTotal: {_evenWinCount}/{_evenCount} (%{evenWinPercent.ToString("0.###")})");
            Console.WriteLine($"Zero: {_zeroCount}");

            Console.WriteLine($"MaxConsecutiveLoss: {_maxConsecutiveLoss}");
            Console.WriteLine($"MaxDiff: {_maxDiff}");

            Console.Read();
        }
        static void Initialization()
        {
            _rand = new Random();
        }
        static void RollDice()
        {
            // Roll olmadan önce seçimimizi yapalım.
            IsOurChoiceOdd();

            // Şimdi rulet dönsün bakalım.
            int result = _rand.Next(0, 47);

            // Sıfır direk kayıp sayılır!!!
            if (result == 0)
            {
                _zeroCount++;
                ConsecutiveLossControl(true);
                return;
            }

            if (result % 2 == 0)
            {
                _isRollChoiceOdd = false;
                _evenCount++;
            }
            else
            {
                _isRollChoiceOdd = true;
                _oddCount++;
            }

            if (_isRollChoiceOdd == _isOurChoiceOdd)
            {
                ConsecutiveLossControl(false);

                if (_isOurChoiceOdd) _oddWinCount++;
                else _evenWinCount++;
            }
            else
            {
                ConsecutiveLossControl(true);
            }
        }
        /// <summary>
        /// Hep tek veya hep çift oynadığımızda 100000 iterasyon içinde maksimum 27 tane kayıp oluyor.
        /// Bir tek bir çift oynadığımızda 100000 iterasyon içinde maksimum 20 tane kayıp oluyor.
        /// Tek veya çifti random seçtiğimizde de 100000 iterasyon içinde maksimum 20 tane kayıp oluyor.
        /// </summary>
        /// <param name="isLoss"></param>
        static void ConsecutiveLossControl(bool isLoss)
        {
            if (isLoss)
            {
                _loseCount++;
                _consecutiveLoss++;
                if (_consecutiveLoss > _maxConsecutiveLoss) _maxConsecutiveLoss = _consecutiveLoss;
                if (_loseCount > _winCount)
                {
                    int temp = _loseCount - _winCount;
                    if (temp > _maxDiff)
                    {
                        _maxDiff = temp;
                    }
                }
            }
            else
            {
                _winCount++;
                _consecutiveLoss = 0;
            }
        }
        static bool IsOurChoiceOdd()
        {
            //_isOurChoiceOdd = !_isOurChoiceOdd;

            //int result = _rand.Next(0, 2);
            //_isOurChoiceOdd = result % 2 != 0;

            return _isOurChoiceOdd;
        }
    }
}